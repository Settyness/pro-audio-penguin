# 🐧 Pro Audio Penguin

<div align="center">
<img src="./img/pro-audio-penguin.png" alt="Pro Audio Penguin" />
</div>

This guide serves as a reference for setting up a Linux audio subsystem to run via a JACK bridge. With decent hardware, this gives the user satisfactory latency and allows for greater control over audio quality and routing. Pipewire is not supported and you will probably run into issues if it is left running on your system. This guide is written with the assumption that you are comfortable with GNU/Linux; package names are based on Arch packages, and you are expected to figure out how to adapt this to your own system.

#### 🚀 Prerequisites

- `alsa-lib` — Where it all begins.
- `alsa-utils` — Controls for ALSA. Optional, but recommended.
- `a2jmidid` — MIDI bridge support. Optional, this guide will not cover MIDI bridges.
- `jack2` — The special sauce for low latency.
- `jack2-dbus` — Bridges to PulseAudio.
- `pulseaudio` — Mixes audio streams.
- `pulseaudio-alsa` — ALSA configuartion for PulseAudio.
- `pulseaudio-bluetooth` Bluetooth support. Optional, but recommended.
- `pulseaudio-jack` — JACK support for PulseAudio.
- `cadence` — Controls JACK session, audio quality and latency, and comes with Catia patchbay.

#### 🔌 Cadence

[KXStudio's](https://kx.studio/) Cadence is the best in class JACK management suite. It has everything you need to manage your JACK settings as well as an incredibly useful software patchbay that's fantastic for on-the-fly routing and live productions.

1. Launch `cadence`.
2. Check "_Auto-start JACK or LADISH at login_".
3. Under **JACK Bridges** -> **ALSA Audio**, set **Bridge Type** to "_ALSA -> PulseAudio -> JACK (Plugin)_".
4. Under **JACK Bridges** -> **PulseAudio**, check "_Auto-start at login_".
5. Click the **Configure** button, this will launch a **JACK Settings** window.
6. Click the **Driver** tab on the up-top, then the **ALSA** tab on the left.
7. Set **Input Device** and **Output Device** to the hardware you wish to utilize.
8. Set **Sample Rate** as desired (44100 is CD quality, 48000 is DVD quality).
9. Set **Buffer Size** as desired (lower is lower latency, but higher DSP).
10. Test the configuration by clicking the **Start** button in the **Cadence** window.

In case you need to make adjustments, the method for restarting is thus:

1. Launch `cadence`.
2. Click the **Stop** button.
3. Click the **Force Restart** button (this will kill PulseAudio).
4. Click the **Start** button.
5. Under **JACK Bridges** -> **PulseAudio**, click the **Start** button.

Use Cadence to monitor Xruns and DSP Load and adjust **Buffer Size** accordingly.

<div align="center">
<img src="./img/cadence.png" alt="Cadence" /><img src="./img/jack-settings.png" alt="JACK Settings" />
</div>

#### 🎧️ PulseAudio

To complete the bridge, we need to set our source and sink to JACK:

```
$ sudo -e /etc/pulse/default.pa
```
```
### Make some devices default
set-default-sink jack_out
set-default-source jack_in
```

PulseAudio will try to spawn these on its own. Comment out these lines or suffer duplicate sources and sinks:

```
### Automatically connect sink and source if JACK server is present
#.ifexists module-jackdbus-detect.so
#.nofail
#load-module module-jackdbus-detect channels=2
#.fail
#.endif
```

If you are using an audio interface, it is highly recommend that you blacklist your integrated audio, as it can steal IRQ priority. Here is my example, your hardware may vary:

```
$ sudo -e /etc/modprobe.d/blacklist.conf
```
```
blacklist snd_hda_intel
```

When you reboot, all of your audio is now routed out via JACK sink and in via JACK source.

<div align="center">
<img src="./img/pavucontrol.png" alt="PulseAudio Volume Control" />
</div>

#### 🧩 Supplemental

The [Professional audio - ArchWiki](https://wiki.archlinux.org/title/Professional_audio) page is a good source for alleged audio latency enhancements. A list of recommended tweaks:

- Set CPU Governor to performance.
- Install `rtirq` and enable the service.
- Install `realtime-privileges` and add your user to the `realtime` group (this may not be necessary with the `rtkit` daemon).
- Lower your system _swappiness_.

It is highly recommend you do not install `linux-rt`. It will cripple your performance with SMT. Modern kernels are sufficient in achieving low audio latencies despite Cadence's protestations.

The [System configuration [Linux-Sound]](https://wiki.linuxaudio.org/wiki/system_configuration) page has a lot more detail. Its similarity to the Arch guide seems to indicate it is likely the source for much of Linux audio dogma.

[`rtcqs`](https://codeberg.org/rtcqs/rtcqs) is a script that makes a couple of rudimentary checks and recommendations in regards to audio latency. Don't worry about satisfying it completely; in fact, the check for mitigations is extremely unhelpful and should be completely disregarded.

[`studiotime`](./studiotime) is my personal script for making temporary adjustments to my system in an effort to avoid xruns during production. Adjust it however you see fit; it is a very simple script.

#### ♻️ Contributing

This guide is open to contributions (specifications, corrections, addendums, etc...) via merge requests and I will also do my best to support anyone who files issues in this repository.
